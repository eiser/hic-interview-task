package main

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"log"
	"net/http"

	"hic-interview-task/controllers"
)

func main() {

	mux := http.NewServeMux()
	mux.HandleFunc("/v1/hello", controllers.HelloV1Handler)
	mux.HandleFunc("/v2/hello", controllers.HelloV2Handler)

	//This implements Let's Encrypt as the certificate provider
	//It's necessary to deploy the code and have a FQDN
	//matching the IP in order for Let's Encrypt to work.
	//
	// certManager := autocert.Manager{
	// 	Prompt: autocert.AcceptTOS,
	// 	Cache:  autocert.DirCache("./certs/tmp"),
	// }
	//
	// server := &http.Server{
	// 	Addr:    ":8443",
	// 	Handler: mux,
	// 	TLSConfig: &tls.Config{
	// 		GetCertificate: certManager.GetCertificate,
	// 	},
	// }
	//
	// go http.ListenAndServe(":80", certManager.HTTPHandler(nil))
	// if err := server.ListenAndServeTLS("", ""); err != nil {
	// 	log.Fatal(err.Error())
	// }

	//Use this to run the code locally without TLS support
	//
	// if err := http.ListenAndServe(":8080", mux); err != nil {
	// 	log.Fatal("Error initializing server...", err)
	// }

	//Using self-signed certificates.
	//
	caCert, err := ioutil.ReadFile("./certs/cacert.pem")
	if err != nil {
		log.Fatal(err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	// TLS Config with the CA pool and enable Client certificate validation
	tlsConfig := &tls.Config{
		ClientCAs:  caCertPool,
		ClientAuth: tls.RequireAndVerifyClientCert,
	}
	tlsConfig.BuildNameToCertificate()

	server := &http.Server{
		Addr:      ":8443",
		TLSConfig: tlsConfig,
		Handler:   mux,
	}

	log.Fatal(server.ListenAndServeTLS("./certs/cert.pem", "./certs/key.pem"))

}
