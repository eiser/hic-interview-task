package controllers

import (
	"encoding/json"
	"hic-interview-task/models"
	"net/http"
	"strings"
)

func HelloV1Handler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/v1/hello" {
		http.Error(w, "404 Page not found.", http.StatusNotFound)
		return
	}

	if r.Method != "GET" {
		http.Error(w, "HTTP method used is not supported.", http.StatusBadRequest)
		return
	}

	ou := r.TLS.PeerCertificates[0].Subject.OrganizationalUnit

	found := false

	for _, v := range ou {
		if strings.ToLower(v) == "unit 1" {
			found = true
			break
		}
	}

	if !found {
		responseError := models.HelloV1ErrorResponse{
			Error: "invalid OU value",
		}
		if err := json.NewEncoder(w).Encode(responseError); err != nil {
			http.Error(w, "Internal Server error...", http.StatusInternalServerError)
		}
		return
	}

	if err := json.NewEncoder(w).Encode("word"); err != nil {
		http.Error(w, "Internal Server error...", http.StatusInternalServerError)
	}
}
