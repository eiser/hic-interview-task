package controllers

import (
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
	"net/http"

	"hic-interview-task/models"
)

func HelloV2Handler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/v2/hello" {
		http.Error(w, "404 Page not found.", http.StatusNotFound)
		return
	}

	if r.Method != "POST" {
		http.Error(w, " HTTP method is not supported.", http.StatusNotFound)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "An error ocurred reading the payload...", http.StatusBadRequest)
		return
	}

	payload, err := models.UnmarshalPayload(body)
	if err != nil {
		http.Error(w, "An error ocurred parsing the payload...", http.StatusBadRequest)
		return
	}

	response := &models.HelloV2Response{
		Data: payload.Format,
	}

	if payload.Format == "json" {
		if err := json.NewEncoder(w).Encode(response); err != nil {
			http.Error(w, "Internal Server error...", http.StatusInternalServerError)
			return
		}
		return
	}

	if payload.Format == "xml" {
		if err := xml.NewEncoder(w).Encode(response); err != nil {
			http.Error(w, "Internal Server error...", http.StatusInternalServerError)
			return
		}
		return
	}

	http.Error(w, "Format not supported, please use 'json' or 'xml'", http.StatusBadRequest)

}
