package models

import "encoding/json"

type Payload struct {
	Format string `json:"format"`
}

type HelloV1ErrorResponse struct {
	Error string `json:"error"`
}

type HelloV2Response struct {
	Data string `json:"data" xml:"data"`
}

func UnmarshalPayload(data []byte) (Payload, error) {
	var r Payload
	err := json.Unmarshal(data, &r)
	return r, err
}
