## Overview
A simple impletation of a TLS webserver that shows how to use a certificate from the request to validate the connection

## How to use
#### You need:
- golang 1.13+
- Docker installed (optional)
- Access to a k8s cluster (optional)

#### Run locally
- Clone the repo
- Compile and run the app:
```
go run main.go
```
or
- Use docker:
```
docker build --build-arg GO_VERSION=1.15 -t <IMAGE_NAME> .
docker run --rm -p 8443:8443 <IMAGE_NAME>:latest
```



