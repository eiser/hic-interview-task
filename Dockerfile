ARG GO_VERSION
FROM golang:${GO_VERSION}-alpine as builder
RUN mkdir /build
ADD . /build/
WORKDIR /build
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o main .

FROM alpine
COPY --from=builder /build/main /app/
RUN mkdir /app/certs
COPY --from=builder /build/certs/cacert.pem /app/certs/
COPY --from=builder /build/certs/cert.pem /app/certs/
COPY --from=builder /build/certs/key.pem /app/certs/
WORKDIR /app
CMD ["./main"]